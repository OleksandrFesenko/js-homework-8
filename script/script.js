// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let textParagraph = document.getElementsByTagName('p');
console.log(textParagraph);

for (const element of textParagraph){
  element.style.background = '#ff0000';
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const listElementId = document.getElementById('optionsList');
console.log(listElementId);

const listElementIdParent = document.getElementById('optionsList').parentNode;
console.log(listElementIdParent);

const listElements = document.getElementById('optionsList').children;
const listElementIdСhildren = listElements
for (element of listElements) {
  console.log(element.nodeName, element.nodeType);
};

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
let testParagraphElement = document.getElementById('testParagraph').textContent = `This is a paragraph`;
console.log(testParagraphElement);

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header').children;
console.log(mainHeader);

const mainHeaderElements = mainHeader
for (element of mainHeader){
  element.className = 'nav-item';
}
console.log(mainHeaderElements);

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitleElements = document.querySelectorAll('.section-title');
console.log(sectionTitleElements);
